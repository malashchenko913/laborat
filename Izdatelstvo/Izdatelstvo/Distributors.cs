﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izdatelstvo
{
    class Distributors:Partner
    {
        private string _TypeDistrebutors = "";
        public string TypeDistrebutors
        {
            get
            {
                return _TypeDistrebutors;
            }
            set
            {
                if (value != null)
                {
                    _TypeDistrebutors = value;
                }
            }
        }
        public virtual void distrebute(Publication _namepublication)
        {
            Console.WriteLine("Я  " + this.Name + " распространил издание " + _namepublication.Name);

        }
    }
}
