﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izdatelstvo
{
    class PrintHouse:Partner
    {
        public virtual void print (Publication _namepublication)
        {
            Console.WriteLine("Я " + this.Name+ " напечатал издание " + _namepublication.Name);

        }
    }
}
