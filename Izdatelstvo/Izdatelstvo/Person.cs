﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izdatelstvo
{
    class Person
    {
        private string _Name = " ";
        private string  _Sername = "";
        private string _Adress = "";
        private long _PhoneNomber = 0;

        public string Name
        {
            get 
            {
                return _Name;
            }
            set 
            {
                if (value != null) 
                {
                    _Name = value;
                }
            
            }
        }

        public string Sername
        {
            get 
            {
                return _Sername;
            }
            set 
            {
                if (value != null) 
                {
                    _Sername = value;              
                }            
            }
        }
        public string Adress
        {
            get 
            {
                return _Adress;
            }
            set 
            {
                if (value != null) 
                {
                    _Adress = value;              
                }            
            }
        }
             public long PhoneNomber
        {
            get 
            {
                return _PhoneNomber;
            }
            set 
            {
                string Nomber = value.ToString();
                if (Nomber.Substring (0) =="8")
                { 
                    if (Nomber.Length == 11)
                    {
                    _PhoneNomber = value;  }            
                }            
            }
        }

    }
    }

